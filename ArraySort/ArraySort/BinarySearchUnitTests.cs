﻿using NUnit.Framework;
namespace ArraySort
{
    [TestFixture]
    public class BinarySearchUnitTests
    {
        int[] array = new int[5] { 10, 20, 30, 40, 50 };
        int arrayLength = 5;

        [Test]
        public void BinarySearch_NumberFoundOnLeftArrayPosition_PositionReturned()
        {
            int searchNumber = 20;
            int result = Program.BinarySearch(array, searchNumber, arrayLength);

            Assert.AreEqual(2, result);
        }

        [Test]
        public void BinarySearch_NumberFoundOnRightArrayPosition_PositionReturned()
        {
            int searchNumber = 50;
            int result = Program.BinarySearch(array, searchNumber, arrayLength);

            Assert.AreEqual(5, result);
        }

        [Test]
        public void BinarySearch_NumberFoundInTheMiddlePosition_PositionReturned()
        {
            int searchNumber = 30;
            int result = Program.BinarySearch(array, searchNumber, arrayLength);

            Assert.AreEqual(3, result);
        }

        [Test]
        public void BinarySearch_NumberNotFound_ErrorValueReturned()
        {
            int searchNumber = 33;
            int result = Program.BinarySearch(array, searchNumber, arrayLength);

            Assert.AreEqual(-1, result);
        }
    }
}
