﻿using NUnit.Framework;

namespace ArraySort
{
    [TestFixture]
    public class RecursiveBinarySearchUnitTests
    {
        int[] array = new int[5] { 10, 20, 30, 40, 50 };
        int arrayLength = 5;

        [Test]
        public void RecursiveBinarySearch_NumberFoundOnLeftArrayPosition_PositionReturned()
        {
            int searchNumber = 20;
            int result = Program.RecursiveBinarySearch(array, searchNumber, arrayLength, 0, arrayLength - 1);

            Assert.AreEqual(2, result);
        }

        [Test]
        public void RecursiveBinarySearch_NumberFoundOnRightArrayPosition_PositionReturned()
        {
            int searchNumber = 50;
            int result = Program.RecursiveBinarySearch(array, searchNumber, arrayLength, 0, arrayLength - 1);

            Assert.AreEqual(5, result);
        }

        [Test]
        public void RecursiveBinarySearch_NumberFoundInTheMiddlePosition_PositionReturned()
        {
            int searchNumber = 30;
            int result = Program.RecursiveBinarySearch(array, searchNumber, arrayLength, 0, arrayLength - 1);

            Assert.AreEqual(3, result);
        }

        [Test]
        public void RecursiveBinarySearch_NumberNotFound_ErrorValueReturned()
        {
            int searchNumber = 33;
            int result = Program.RecursiveBinarySearch(array, searchNumber, arrayLength, 0, arrayLength - 1);

            Assert.AreEqual(-1, result);
        }
    }
}
