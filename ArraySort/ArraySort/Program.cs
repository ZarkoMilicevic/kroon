﻿using System;
using System.Configuration;

namespace ArraySort
{
    public class Program
    {
        static void Main(string[] args)
        {
            bool isLengthParsed;

            Console.WriteLine("Enter Array Lentgth:");
            string arrayLengthString = Console.ReadLine();
            isLengthParsed = int.TryParse(arrayLengthString, out int arrayLength);

            Console.WriteLine("Enter Array Elements in sorted order:");
            int[] array = new int[arrayLength];
            for (int i=0; i < arrayLength; i++)
            {
                array[i] = int.Parse(Console.ReadLine());
            }

            Console.WriteLine("Enter the number to be searched: ");
            int number = int.Parse(Console.ReadLine());

            int result = GetNumberPosition(array, number, arrayLength);

            Console.WriteLine(result == -1 ? "Not Found" : $"Position is {result}");
            Console.ReadLine();
        }


        private static int GetNumberPosition(int[] array, int number, int arrayLength)
        {
            string recursiveParam = ConfigurationManager.AppSettings["UseRecursiveSearch"];
            bool useRecursive = bool.Parse(recursiveParam);
            if (useRecursive)
            {
                return BinarySearch(array, number, arrayLength);
            }
            else
            {
                return RecursiveBinarySearch(array, number, arrayLength, 0, arrayLength - 1);
            }
        }

        public static int BinarySearch(int[] array, int number, int arrayLength)
        {
            int min = 0;
            int max = arrayLength - 1;
            int mid = 0;

            while(min <= max)
            {
                mid = (min + max) / 2;

                if(number < array[mid])
                {
                    max = mid - 1;
                }
                else if(number > array[mid])
                {
                    min = mid + 1;
                }
                else if(number == array[mid])
                {
                    return mid + 1;
                }

            }
            return -1 ;
        }

        public static int RecursiveBinarySearch(int[] array, int number, int arrayLength, int min, int max)
        {
            int mid = (min + max) / 2;

            if(min > max)
            {
                return -1;
            }
            else
            {
                if (number == array[mid])
                {
                    return mid + 1;
                }
                else if (number < array[mid])
                {
                    return RecursiveBinarySearch(array, number, arrayLength, min, mid - 1);
                }
                else
                {
                    return RecursiveBinarySearch(array, number, arrayLength, mid + 1, max);
                }
            }
        }
    }
}
